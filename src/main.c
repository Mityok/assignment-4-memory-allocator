//
// Created by mitya on 24.12.2023.
//


#include "assert.h"
#include "mem.h"
#include <stdio.h>


void printNumberOftest(char str[]){
    printf("Test #");
    printf("%s", str);
}

void default_succesful_memory_allocation(){
    printNumberOftest("1");

    struct region *myHeap = heap_init(4096);
    assert(myHeap);
    heap_term();

    printf("Test Passed");
}

void free_one_block_from_many(){
    printNumberOftest("2");

    void* myHeap = heap_init(4096);
    assert(myHeap);

    void* new_block_1 = _malloc(64);
    void* new_block_2 = _malloc(64);
    void* new_block_3 = _malloc(64);
    void* new_block_4 = _malloc(64);

    assert(new_block_1);
    assert(new_block_2);
    assert(new_block_3);
    assert(new_block_4);

    _free(new_block_1);


    assert(new_block_2);
    assert(new_block_3);
    assert(new_block_4);

    heap_term();

    printf("Test Passed");
}


void free_two_blocks_from_many(){
    printNumberOftest("3");

    void* myHeap = heap_init(4096);
    assert(myHeap);

    void* new_block_1 = _malloc(64);
    void* new_block_2 = _malloc(64);
    void* new_block_3 = _malloc(64);
    void* new_block_4 = _malloc(64);

    assert(new_block_1);
    assert(new_block_2);
    assert(new_block_3);
    assert(new_block_4);

    _free(new_block_1);
    _free(new_block_2);

    assert(new_block_3);
    assert(new_block_4);

    heap_term();

    printf("Test Passed");
}

void new_region_expands_old_region(){
    printNumberOftest("4");

    heap_init(0);


    void *new_block1 = _malloc(1024);
    void *new_block2 = _malloc(2048);
    assert(new_block1);
    assert(new_block2);

    _free(new_block1);
    _free(new_block2);

    heap_term();
    printf("Test Passed");

}

void new_region_none_expands_old_region(){
    printNumberOftest("5");

    void *addres = mmap(HEAP_START, 10000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
    void *my_block = _malloc(64);


    assert(my_block);

    _free(my_block);
    _free(addres);

    printf("Test Passed");
}

int main(){
    //Обычное успешное выделение памяти
    default_succesful_memory_allocation();
    //Освобождение одного блока из нескольких выделенных.
    free_one_block_from_many();
    //Освобождение двух блоков из нескольких выделенных.
    free_two_blocks_from_many();
    //Память закончилась, новый регион памяти расширяет старый.
    new_region_expands_old_region();
    //Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
    new_region_none_expands_old_region();
    return 0;
}